// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ModuleManager.h"

class IIOSPluginModule : public IModuleInterface
{
public:
    static inline IIOSPluginModule& Get()
    {
        return FModuleManager::LoadModuleChecked< IIOSPluginModule >("IOSPlugin");
    }
    
    static inline bool IsAvailable()
    {
        return FModuleManager::Get().IsModuleLoaded("IOSPlugin");
    }
};
