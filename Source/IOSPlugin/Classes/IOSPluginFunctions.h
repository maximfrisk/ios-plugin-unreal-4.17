// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ModuleManager.h"
#include "UObject/Object.h"
#include "UObject/ScriptMacros.h"
#include "IOSPluginFunctions.generated.h"

#if PLATFORM_IOS
#import <UIKit/UIKit.h>

@interface iOSPlugin : UIViewController
+ (instancetype)GetSingelton;

@end
#endif

UENUM(BlueprintType)
enum class EFeedbackType :uint8 {
    Peek        UMETA(DisplayName = "Peek"),
    Pop         UMETA(DisplayName = "Pop"),
    Nope        UMETA(DisplayName = "Nope")
};

UCLASS(BlueprintType)
class IOSPLUGIN_API UIOSPluginFunctions : public UObject {
    GENERATED_BODY()
    
    public:
    
    UFUNCTION(BlueprintCallable, Category = "IOS Plugin")
    static void PlayFeedback(EFeedbackType type);
    
    UFUNCTION(BlueprintCallable, Category = "IOS Plugin")
    static bool IsIphoneX();
};
