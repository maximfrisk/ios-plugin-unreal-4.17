// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "IOSPlugin.h"
#include "EngineMinimal.h"
#include "CoreMinimal.h"

#define LOCTEXT_NAMESPACE "FIOSPluginModule"

class FIOSPluginModule : public IIOSPluginModule
{
    virtual void StartupModule() override;
    virtual void ShutdownModule() override;
};

IMPLEMENT_MODULE(FIOSPluginModule, IOSPlugin)

// Startup Module
void FIOSPluginModule::StartupModule()
{
    UE_LOG(LogTemp, Log, TEXT("StartupModule IOSPlugin"));
}

// Shutdown Module
void FIOSPluginModule::ShutdownModule()
{
     UE_LOG(LogTemp, Log, TEXT("ShutdownModule IOSPlugin"));
}


#undef LOCTEXT_NAMESPACE
