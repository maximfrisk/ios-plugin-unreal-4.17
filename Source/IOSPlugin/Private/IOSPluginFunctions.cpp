// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "IOSPluginFunctions.h"

#if PLATFORM_IOS
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>

@interface iOSPlugin()

@end

@implementation iOSPlugin

-(instancetype)init
{
    if(self = [super init]){
    }
    return self;
}

+ (instancetype)GetSingelton
{
    static iOSPlugin *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[iOSPlugin alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void)FeedbackPeek
{
    //Actuate `Peek` feedback (weak boom)
    AudioServicesPlaySystemSound(1519);
}

- (void)FeedbackPop
{
    //Actuate `Pop` feedback (strong boom)
    AudioServicesPlaySystemSound(1520);
}

- (void)FeedbackNope
{
    //Actuate `Nope` feedback (series of three weak booms)
    AudioServicesPlaySystemSound(1521);
}

@end
#endif


/**
UIOSPluginFunctions Start
__________________________
*/

void UIOSPluginFunctions::PlayFeedback(EFeedbackType type)
{
#if PLATFORM_IOS
    switch(type){
        case EFeedbackType::Peek: {
            [[iOSPlugin GetSingelton] FeedbackPeek];
            break;
        }
        case EFeedbackType::Pop: {
            [[iOSPlugin GetSingelton] FeedbackPop];
            break;
        }
        case EFeedbackType::Nope: {
            [[iOSPlugin GetSingelton] FeedbackNope];
            break;
        }
    }
#endif
}

bool UIOSPluginFunctions::IsIphoneX()
{
#if PLATFORM_IOS
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && UIScreen.mainScreen.nativeBounds.size.height == 2436)  {
        //iPhone X
        return true;
    }
#endif
    return false;
}
